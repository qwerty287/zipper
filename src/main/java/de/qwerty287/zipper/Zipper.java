package de.qwerty287.zipper;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * A simple class to work with ZIP archives.
 * <ul>
 *     <li>create archives</li>
 *     <li>add files</li>
 *     <li>read archives</li>
 * </ul>
 * <br>
 * Note: always call {@link #close()} if you finished adding your files!
 * <br><br>
 * To create a new instance:
 * <blockquote><pre>
 * Zipper z = new Zipper("file.zip");
 * </pre></blockquote>
 * You can also use {@link java.io.File} objects as argument if you can use file paths.
 * <br><br>
 * To write to a file, enter write mode and add files with the {@link #add(String)}, {@link #add(File)} and
 * {@link #add(InputStream, ZipEntry)} methods:
 * <blockquote><pre>
 * z.write()
 *         .add("path/to/file.txt")
 *         .add(new File("path/to/folder/")
 *         .close();
 * </pre></blockquote>
 * If you do not want to create a new file if it does not exist (will throw a {@link
 * FileNotFoundException} if write mode is entered), use {@link #write(boolean)}
 * and set the parameter to <code>false</code>.
 * <br>
 * If adding folders, all files in the folder will be added.
 * All setters can be used in chained calls.
 * <br>
 * Always call {@link #close()} after adding the files (you won't be able to use read methods like {@link #read()} until
 * you called {@link #close()}).
 * <p>
 * To read all entries of a ZIP file use {@link #read()} (returns a {@link java.util.List} of
 * {@link java.util.zip.ZipEntry}s), to get only one entry use {@link #read(String)}:
 * <blockquote><pre>
 * ZipEntry firstEntry = z.read().get(0);
 * ZipEntry entry = z.read("path/to/file/in/archive.txt");
 * </pre></blockquote>
 * You should also call {@link #close()} after reading to close the {@link java.util.zip.ZipFile}.
 * <p>
 * {@link de.qwerty287.zipper.Zipper} has an internal {@link #unzip()} method which will unzip the archive to the
 * current working directory:
 * <blockquote><pre>
 * z.unzip();
 * z.unzip("directory/to/unzip/");
 * </pre></blockquote>
 *
 * @author qwerty287
 * @version 1.0.0-SNAPSHOT
 */
public class Zipper {

    private final File file;
    private final Charset charset;
    private OutputStream fos;
    private ZipOutputStream zos;
    private boolean write = false;
    private ZipFile zipFile;

    /**
     * Create a new {@link de.qwerty287.zipper.Zipper} with {@link java.nio.charset.StandardCharsets#UTF_8}.
     *
     * @param filepath the path where the archive will be created
     * @since 1.0.0
     */
    public Zipper(String filepath) {
        this(new File(filepath));
    }

    /**
     * Create a new {@link de.qwerty287.zipper.Zipper} with {@link java.nio.charset.StandardCharsets#UTF_8}.
     *
     * @param file the {@link java.io.File} where the archive will be created
     * @since 1.0.0
     */
    public Zipper(File file) {
        this(file, StandardCharsets.UTF_8);
    }

    /**
     * Create a new {@link de.qwerty287.zipper.Zipper}.
     *
     * @param charset  the {@link java.nio.charset.Charset} that will be used when reading the file.
     * @param filepath the path where the archive will be created
     * @since 1.0.0-SNAPSHOT
     */
    public Zipper(String filepath, Charset charset) {
        this(new File(filepath), charset);
    }

    /**
     * Create a new {@link de.qwerty287.zipper.Zipper}.
     *
     * @param charset the {@link java.nio.charset.Charset} that will be used when reading the file.
     * @param file    the {@link java.io.File} where the archive will be created
     * @since 1.0.0-SNAPSHOT
     */
    public Zipper(File file, Charset charset) {
        this.file = file;
        this.charset = charset;
    }

    /**
     * Read an {@link java.io.InputStream}. Similar to {@link #read()}, but does not depend on {@link java.io.File} objects. Reads using
     * {@link java.nio.charset.StandardCharsets#UTF_8}.
     * Note: loads the ZIP file, and it's content completely. Not recommended for big files.
     *
     * @param stream the {@link java.io.InputStream}
     * @return the list of {@link java.util.zip.ZipEntry}s
     * @throws java.io.IOException if something went wrong while reading the {@link java.io.InputStream}
     * @since 1.0.0-SNAPSHOT
     */
    public static HashMap<ZipEntry, List<Byte>> read(InputStream stream) throws IOException {
        return read(stream, StandardCharsets.UTF_8);
    }

    /**
     * Read an {@link java.io.InputStream}. Similar to {@link #read()}, but does not depend on {@link java.io.File} objects.
     * Note: loads the ZIP file, and it's content completely. Not recommended for big files.
     *
     * @param stream  the {@link java.io.InputStream}
     * @param charset the {@link java.nio.charset.Charset} to create the {@link java.util.zip.ZipInputStream} and read the file
     * @return the list of {@link java.util.zip.ZipEntry}s
     * @throws java.io.IOException if something went wrong while reading the {@link java.io.InputStream}
     * @since 1.0.0-SNAPSHOT
     */
    public static HashMap<ZipEntry, List<Byte>> read(InputStream stream, Charset charset) throws IOException {
        ZipInputStream zis;
        if (stream instanceof ZipInputStream) {
            zis = (ZipInputStream) stream;
        } else {
            zis = new ZipInputStream(stream, charset);
        }
        HashMap<ZipEntry, List<Byte>> entries = new HashMap<>();
        ZipEntry ze;
        while ((ze = zis.getNextEntry()) != null) {
            ArrayList<Byte> bytes = new ArrayList<>();
            int b;
            while ((b = zis.read()) > 0) {
                bytes.add((byte) b);
            }
            entries.put(ze, bytes);
            zis.closeEntry();
        }
        return entries;
    }

    /**
     * Enter write mode to edit the ZIP file. If the file already exists data will be overwritten;
     * if not, a new file will be created.
     *
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @throws java.io.IOException if something went wrong while initializing the required fields or creating the file
     * @since 1.0.0
     */
    public Zipper write() throws IOException {
        return write(true);
    }

    /**
     * Enter write mode to edit the ZIP file. If the file already exists data will be overwritten.
     *
     * @param createNewFile if a new file should be created
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @throws java.io.IOException if something went wrong while initializing the required fields or creating the file
     * @since 1.0.0
     */
    public Zipper write(boolean createNewFile) throws IOException {
        reqReadMode(false);
        if (!file.exists()) {
            if (createNewFile && !file.createNewFile()) {
                throw new IOException("Could not create file");
            } else if (!createNewFile) {
                throw new FileNotFoundException("File does not exist");
            }
        }
        return write(new FileOutputStream(file));
    }

    /**
     * Enter write mode to edit the ZIP file. If the file already exists data will be overwritten.
     *
     * @param stream the {@link java.io.OutputStream} that is used to write to the ZIP file
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @throws java.io.IOException if something went wrong while closing the {@link java.util.zip.ZipFile}
     * @since 1.0.0-SNAPSHOT
     */
    public Zipper write(OutputStream stream) throws IOException {
        reqReadMode(false);
        if (zipFile != null) {
            zipFile.close();
            zipFile = null;
        }
        write = true;
        if (stream instanceof ZipOutputStream) {
            zos = (ZipOutputStream) stream;
        } else {
            fos = stream;
            zos = new ZipOutputStream(fos, charset);
        }
        return this;
    }

    /**
     * Add the given {@link java.io.File} to the ZIP archive.
     *
     * @param file the {@link java.io.File} that will be added
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @throws java.io.IOException if the file does not exist or something went wrong
     * @since 1.0.0
     */
    public Zipper add(File file) throws IOException {
        return add(file, file.getPath());
    }

    /**
     * Add the file at the given file path to the ZIP archive.
     *
     * @param filePath the path to the file
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @throws java.io.IOException if the file does not exist or something went wrong
     * @since 1.0.0
     */
    public Zipper add(String filePath) throws IOException {
        return add(new File(filePath));
    }

    /**
     * Add the given {@link java.io.File} to the ZIP archive.
     *
     * @param file the {@link java.io.File} that will be added
     * @param name the name of the file in the archive
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @throws java.io.IOException if the file does not exist or something went wrong
     * @since 1.0.0
     */
    public Zipper add(File file, String name) throws IOException {
        reqWriteMode();
        if (!file.exists()) throw new FileNotFoundException("File does not exist");

        if (file.isDirectory()) {
            addDir(file, name);
        } else if (file.isFile()) {
            addSingleFile(file, name);
        }
        return this;
    }

    /**
     * Add the file at the given file path to the ZIP archive.
     *
     * @param filePath the path to the file
     * @param name     the name of the file in the archive
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @throws java.io.IOException if the file does not exist or something went wrong
     * @since 1.0.0
     */
    public Zipper add(String filePath, String name) throws IOException {
        return add(new File(filePath), name);
    }

    /**
     * Add the {@link java.io.InputStream} to the ZIP archive.
     *
     * @param stream the {@link java.io.InputStream}
     * @param name   the name of the file in the archive
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @throws java.io.IOException if the file does not exist or something went wrong
     * @since 1.0.0-SNAPSHOT
     */
    public Zipper add(InputStream stream, String name) throws IOException {
        return add(stream, new ZipEntry(name));
    }

    /**
     * Add a {@link java.util.zip.ZipEntry} to the ZIP file. This method allows more control over the compressing
     * methods and other things.
     *
     * @param filepath the filepath of the file
     * @param file     the {@link java.util.zip.ZipEntry}
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @throws java.io.IOException if something went wrong while writing the file
     * @since 1.0.0-SNAPSHOT
     */
    public Zipper add(String filepath, ZipEntry file) throws IOException {
        return add(new FileInputStream(filepath), file);
    }

    /**
     * Add a {@link java.util.zip.ZipEntry} to the ZIP file. This method allows more control over the compressing
     * methods and other things.
     *
     * @param file    the {@link java.io.File} that will be added
     * @param zipFile the {@link java.util.zip.ZipEntry}
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @throws java.io.IOException if something went wrong while writing the file
     * @since 1.0.0-SNAPSHOT
     */
    public Zipper add(File file, ZipEntry zipFile) throws IOException {
        return add(new FileInputStream(file), zipFile);
    }

    /**
     * Add a {@link java.util.zip.ZipEntry} to the ZIP file. This method allows more control over the compressing
     * methods and other things.
     *
     * @param stream the {@link java.io.InputStream} that will be used to write into the ZIP file
     * @param file   the {@link java.util.zip.ZipEntry}
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @throws java.io.IOException if something went wrong while writing the file
     * @since 1.0.0
     */
    public Zipper add(InputStream stream, ZipEntry file) throws IOException {
        addSingleFile(file, stream);
        return this;
    }

    private void addDir(File dir, String name) {
        if (!dir.isDirectory()) throw new IllegalArgumentException("File is not a directory");
        try {
            ArrayList<File> files = getFilesList(dir);

            for (File file : files) {
                addSingleFile(file, file.getPath().replace(dir.getName(), name));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private ArrayList<File> getFilesList(File dir) throws IOException {
        ArrayList<File> filesOutput = new ArrayList<>();
        File[] files = dir.listFiles();
        assert files != null;
        for (File file : files) {
            if (file.isFile()) {
                filesOutput.add(file);
            } else {
                filesOutput.addAll(getFilesList(file));
            }
        }
        return filesOutput;
    }

    private void addSingleFile(File file, String name) throws IOException {
        reqWriteMode();
        if (!file.isFile()) throw new IllegalArgumentException("File is not a file");
        ZipEntry ze = new ZipEntry(name);
        FileInputStream fis = new FileInputStream(file);
        addSingleFile(ze, fis);
    }

    private void addSingleFile(ZipEntry file, InputStream stream) throws IOException {
        reqWriteMode();
        zos.putNextEntry(file);
        byte[] buffer = new byte[1024];
        int len;
        while ((len = stream.read(buffer)) > 0) {
            zos.write(buffer, 0, len);
        }

        zos.closeEntry();
        stream.close();
    }

    /**
     * Close the resources. Always call this if your files are added, or you read all files! If the
     * {@link de.qwerty287.zipper.Zipper} is currently in write mode, the {@link java.util.zip.ZipOutputStream} and
     * {@link java.io.FileOutputStream} will be closed, if it is in read mode, the {@link java.util.zip.ZipFile} will be
     * closed.
     *
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @throws java.io.IOException if something went wrong while closing the resources
     * @since 1.0.0
     */
    public Zipper close() throws IOException {
        if (write) {
            write = false;
            zos.close();
            if (fos != null) fos.close();
            zos = null;
            fos = null;
        } else if (zipFile != null) {
            zipFile.close();
            zipFile = null;
        }
        return this;
    }

    /**
     * Open the archive and get the bytes.
     *
     * @return the bytes list
     * @throws java.io.IOException if something went wrong while opening or reading the file
     * @since 1.0.0
     */
    public byte[] getBytes() throws IOException {
        reqReadMode();
        FileInputStream fis = new FileInputStream(file);
        byte[] bytes = new byte[0];
        int i;
        while ((i = fis.read()) != -1) {
            byte[] newBytes = new byte[bytes.length + 1];
            System.arraycopy(bytes, 0, newBytes, 0, bytes.length);
            newBytes[bytes.length] = (byte) i;
            bytes = newBytes;
        }
        return bytes;
    }

    /**
     * The {@link java.io.File} object of the archive.
     *
     * @return the {@link java.io.File} of the archive
     * @since 1.0.0
     */
    public File getFile() {
        return file;
    }

    /**
     * Read the ZIP file.
     *
     * @return the list of {@link java.util.zip.ZipEntry}s
     * @throws java.io.IOException if something went wrong while reading the file
     * @since 1.0.0
     */
    public List<ZipEntry> read() throws IOException {
        reqReadMode();
        List<ZipEntry> entries = new ArrayList<>();
        ZipFile f = getZipFile();
        Enumeration<? extends ZipEntry> zs = f.entries();
        while (zs.hasMoreElements()) {
            entries.add(zs.nextElement());
        }
        return entries;
    }

    /**
     * Read the {@link java.util.zip.ZipEntry} with the given name.
     *
     * @param name the name of the required {@link java.util.zip.ZipEntry}
     * @return the {@link java.util.zip.ZipEntry}
     * @throws java.io.IOException if something went wrong while creating the {@link java.util.zip.ZipFile} object
     * @since 1.0.0
     */
    public ZipEntry read(String name) throws IOException {
        reqReadMode();
        return getZipFile().getEntry(name);
    }

    /**
     * Unzip the ZIP file.
     *
     * @param target the target directory in which the files will be unpacked
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @throws java.io.IOException if something went wrong while reading the file
     * @since 1.0.0
     */
    public Zipper unzip(String target) throws IOException {
        return unzip(new File(target));
    }

    /**
     * Unzip the ZIP file.
     *
     * @param target the target directory in which the files will be unpacked
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @throws java.io.IOException if something went wrong while reading the file
     * @since 1.0.0
     */
    public Zipper unzip(File target) throws IOException {
        reqReadMode();
        List<ZipEntry> entries = read();

        ZipFile f = getZipFile();
        if (!target.mkdirs() && !target.exists()) {
            throw new IOException("Could not create required directories");
        }

        for (ZipEntry z : entries) {
            InputStream in = f.getInputStream(z);
            ArrayList<String> path = new ArrayList<>(Arrays.asList(z.getName().split("/")));
            String fileName = path.remove(path.size() - 1);
            String pathDirs = String.join("/", path);
            File file = new File(target, pathDirs);
            if (!file.mkdirs() && !file.exists()) {
                throw new IOException("Could not create required directories");
            }
            OutputStream out = new BufferedOutputStream(new FileOutputStream(new File(file, fileName)));
            byte[] buffer = new byte[1024];
            int len;
            while ((len = in.read(buffer)) >= 0) {
                out.write(buffer, 0, len);
            }
            in.close();
            out.close();
        }

        return this;
    }

    /**
     * Unzip the ZIP file in the current working directory.
     *
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @throws java.io.IOException if something went wrong while reading the file
     * @since 1.0.0
     */
    public Zipper unzip() throws IOException {
        return unzip("./");
    }

    /**
     * Create a {@link java.util.zip.ZipFile} for the ZIP file, using the default {@link java.nio.charset.Charset} (set
     * in constructor).
     *
     * @return the generated {@link java.util.zip.ZipFile}
     * @throws java.io.IOException if something went wrong while creating the {@link java.util.zip.ZipFile} object
     * @since 1.0.0
     */
    public ZipFile getZipFile() throws IOException {
        reqReadMode();
        if (zipFile == null) {
            zipFile = new ZipFile(file, charset);
        }
        return zipFile;
    }

    /**
     * Set the {@link java.util.zip.ZipFile} which is used for various methods like {@link #read()}. This method is
     * intended to use if you can't work with {@link java.io.File}s or if you want to create the
     * {@link java.util.zip.ZipFile} with more configuration options than {@link #getZipFile()} would create it
     * ({@link #getZipFile()} uses the {@link java.util.zip.ZipFile#ZipFile(File, Charset)} constructor).
     *
     * @param zipFile the {@link java.util.zip.ZipFile}
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @since 1.0.0-SNAPSHOT
     */
    public Zipper setZipFile(ZipFile zipFile) {
        this.zipFile = zipFile;
        return this;
    }

    /**
     * Get the ZIP file comment.
     *
     * @return the file's comment itself
     * @throws java.io.IOException if something went wrong while creating the {@link java.util.zip.ZipFile} object
     * @since 1.0.0
     */
    public String getComment() throws IOException {
        return getZipFile().getComment();
    }

    /**
     * Set the ZIP file comment.
     *
     * @param comment the comment
     * @return the {@link de.qwerty287.zipper.Zipper} itself
     * @since 1.0.0
     */
    public Zipper setComment(String comment) {
        reqWriteMode();
        zos.setComment(comment);
        return this;
    }

    /**
     * Check if the {@link de.qwerty287.zipper.Zipper} is currently in write mode.
     *
     * @return if the {@link de.qwerty287.zipper.Zipper} is currently in write mode.
     * @since 1.0.0
     */
    public boolean isWrite() {
        return write;
    }

    /**
     * Get the {@link java.nio.charset.Charset} configured for this {@link de.qwerty287.zipper.Zipper}.
     *
     * @return the {@link java.nio.charset.Charset} configured in the constructor
     * @since 1.0.0-SNAPSHOT
     */
    public Charset getCharset() {
        return charset;
    }

    /**
     * Open an {@link java.io.InputStream} for the given {@link java.util.zip.ZipEntry}.
     *
     * @param zipEntry the {@link java.util.zip.ZipEntry} to read
     * @return the {@link java.io.InputStream} with the {@link java.util.zip.ZipEntry}'s content
     * @throws java.io.IOException if something went wrong while create the {@link java.util.zip.ZipFile}
     */
    public InputStream openStream(ZipEntry zipEntry) throws IOException {
        return getZipFile().getInputStream(zipEntry);
    }

    /**
     * Open an {@link java.io.InputStream} for the given filename.
     *
     * @param file the filename of the file to read
     * @return the {@link java.io.InputStream} with the file's content
     * @throws java.io.IOException if something went wrong while create the {@link java.util.zip.ZipFile}
     */
    public InputStream openStream(String file) throws IOException {
        return openStream(getZipFile().getEntry(file));
    }

    private void reqWriteMode() {
        if (!write || zos == null || fos == null) {
            throw new IllegalStateException("Zipper is not in write mode. Use write() to enter write mode");
        }
    }

    private void reqReadMode() throws FileNotFoundException {
        reqReadMode(true);
    }

    private void reqReadMode(boolean throwFileNotFound) throws FileNotFoundException {
        if (write && zos != null && fos != null) {
            throw new IllegalStateException("Zipper is not in read mode. Use close() to exit write mode");
        }
        if (throwFileNotFound && !file.exists()) {
            throw new FileNotFoundException("File does not exist");
        }
    }

}
