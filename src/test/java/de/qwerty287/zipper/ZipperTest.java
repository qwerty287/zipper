package de.qwerty287.zipper;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ZipperTest {

    private static final Zipper z = new Zipper("zippertest.zip");
    private static final File f = new File("examplefile.txt");
    private static final File unzipFolder = new File("unziptest");
    private static final String comment = "zip test file comment";

    @AfterClass
    public static void clean() throws IOException {
        // delete unzipped files
        for (File i : Objects.requireNonNull(unzipFolder.listFiles())) {
            if (!i.delete()) {
                System.out.println("did not delete unzipped file " + i.getPath());
            }
        }
        if (!unzipFolder.delete()) {
            System.out.println("did not delete unzip folder");
        }

        // delete unzipped files in work directory
        for (ZipEntry i : z.read()) {
            if (!new File(i.getName()).delete()) {
                System.out.println("did not delete unzipped file " + i.getName());
            }
        }

        // close Zipper
        z.close();

        // delete test files
        if (!f.delete()) {
            System.out.println("did not delete temporary file");
        }
        if (!z.getFile().delete()) {
            System.out.println("did not delete ZIP file");
        }
    }

    @Before
    public void prepare() throws IOException {
        if (!f.createNewFile() && !f.exists()) {
            throw new FileNotFoundException();
        }
        FileOutputStream fos = new FileOutputStream(f);
        fos.write("examplecontent\n".getBytes(StandardCharsets.UTF_8));
        fos.close();

        z.write()
                .add("examplefile.txt", "nameinzip.txt")
                .add("examplefile.txt")
                .add(f.getPath(), new ZipEntry("zipentryfile.txt"))
                .add(f, new ZipEntry("zipentryfilewithfile.txt"))
                .add(new FileInputStream(f), "filefromstream.txt")
                .setComment(comment)
                .close();
    }

    @Test
    public void baseTest() {
        Assert.assertEquals(z.getCharset(), StandardCharsets.UTF_8);
        Assert.assertFalse(z.isWrite());
    }

    @Test
    public void readTest() throws IOException {
        Assert.assertEquals(z.getComment(), comment);
        Assert.assertEquals(z.read().size(), 5);
        Assert.assertEquals(z.read().get(2).getName(), "zipentryfile.txt");
        Assert.assertEquals(z.read("examplefile.txt").getMethod(), ZipEntry.DEFLATED);
        Assert.assertEquals(z.getBytes().length, 760);
    }

    @Test
    public void unzipTest() throws IOException {
        if (!unzipFolder.mkdirs() && !unzipFolder.exists()) {
            System.out.println("could not create file");
        }

        z.unzip(unzipFolder);
    }

    @Test
    public void unzipWorkDirTest() throws IOException {
        z.unzip();
    }

    @Test
    public void setZipFileTest() throws IOException {
        Zipper zipperLocal = z;
        Zipper zipper2 = new Zipper("examplezip2.zip")
                .write()
                .add(f)
                .close();
        Assert.assertEquals(zipperLocal.setZipFile(new ZipFile(zipper2.getFile())), zipperLocal);
        // if we now read the file, it will read examplezip2.zip
        Assert.assertEquals(zipperLocal.read().size(), 1);

        if (!zipper2.getFile().delete()) {
            System.out.println("did not delete second ZIP file");
        }
    }

    @Test
    public void staticTest() throws IOException {
        HashMap<ZipEntry, List<Byte>> zipEntryList = Zipper.read(new FileInputStream(z.getFile()));
        Assert.assertEquals(zipEntryList.size(), 5);

        for (ZipEntry i : zipEntryList.keySet()) {
            Assert.assertEquals((byte) zipEntryList.get(i).get(0), "e".getBytes()[0]);
        }
    }

    @Test
    public void openStreamTest() throws IOException {
        Assert.assertEquals(z.openStream("zipentryfile.txt").read(), "e".getBytes()[0]);
    }

}
