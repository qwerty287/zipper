# Zipper

A simple Java library to work with ZIP files.

For documentation, see https://qwerty287.codeberg.page/zipper (Javadoc generated).

To use this in your project, you have some possibilities:
1. download the
[`Zipper` class](https://codeberg.org/qwerty287/zipper/src/branch/main/src/main/java/de/qwerty287/zipper/Zipper.java)
and add it to your project. You are also able to change the file according to your needs.
2. If you are using a Maven compatible build system, you can [get Zipper
from JitPack](https://jitpack.io/#org.codeberg.qwerty287/zipper).
3. Or you download the JAR file from the releases page.

### Build

Zipper uses [Maven](https://maven.apache.org/), which is required to build and test it using this way:

| Usage               | Command               |
|---------------------|-----------------------|
| Build files         | `mvn compile`         |
| Run test            | `mvn test`            |
| Clean files         | `mvn clean`           |
| Generate Javadoc    | `mvn javadoc:javadoc` |
| Fix/Improve Javadoc | `mvn javadoc:fix`     |
| Build JAR           | `mvn package`         |
